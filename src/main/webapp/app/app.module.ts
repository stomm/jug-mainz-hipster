import './vendor.ts';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Ng2Webstorage } from 'ngx-webstorage';

import { JugMainzHipsterSharedModule, UserRouteAccessService } from './shared';
import { JugMainzHipsterAppRoutingModule} from './app-routing.module';
import { JugMainzHipsterHomeModule } from './home/home.module';
import { JugMainzHipsterAdminModule } from './admin/admin.module';
import { JugMainzHipsterAccountModule } from './account/account.module';
import { JugMainzHipsterEntityModule } from './entities/entity.module';
import { customHttpProvider } from './blocks/interceptor/http.provider';
import { PaginationConfig } from './blocks/config/uib-pagination.config';

// jhipster-needle-angular-add-module-import JHipster will add new module here

import {
    JhiMainComponent,
    NavbarComponent,
    FooterComponent,
    ProfileService,
    PageRibbonComponent,
    ActiveMenuDirective,
    ErrorComponent
} from './layouts';

@NgModule({
    imports: [
        BrowserModule,
        JugMainzHipsterAppRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-'}),
        JugMainzHipsterSharedModule,
        JugMainzHipsterHomeModule,
        JugMainzHipsterAdminModule,
        JugMainzHipsterAccountModule,
        JugMainzHipsterEntityModule,
        // jhipster-needle-angular-add-module JHipster will add new module here
    ],
    declarations: [
        JhiMainComponent,
        NavbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        ActiveMenuDirective,
        FooterComponent
    ],
    providers: [
        ProfileService,
        customHttpProvider(),
        PaginationConfig,
        UserRouteAccessService
    ],
    bootstrap: [ JhiMainComponent ]
})
export class JugMainzHipsterAppModule {}
