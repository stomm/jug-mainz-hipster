import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { ProductJugComponent } from './product-jug.component';
import { ProductJugDetailComponent } from './product-jug-detail.component';
import { ProductJugPopupComponent } from './product-jug-dialog.component';
import { ProductJugDeletePopupComponent } from './product-jug-delete-dialog.component';

export const productRoute: Routes = [
    {
        path: 'product-jug',
        component: ProductJugComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jugMainzHipsterApp.product.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'product-jug/:id',
        component: ProductJugDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jugMainzHipsterApp.product.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const productPopupRoute: Routes = [
    {
        path: 'product-jug-new',
        component: ProductJugPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jugMainzHipsterApp.product.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'product-jug/:id/edit',
        component: ProductJugPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jugMainzHipsterApp.product.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'product-jug/:id/delete',
        component: ProductJugDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jugMainzHipsterApp.product.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
