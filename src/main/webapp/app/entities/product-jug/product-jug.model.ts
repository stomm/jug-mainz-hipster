import { BaseEntity } from './../../shared';

export const enum Currency {
    'EUR',
    'USD'
}

export class ProductJug implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public price?: number,
        public currency?: Currency,
    ) {
    }
}
