import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JugMainzHipsterSharedModule } from '../../shared';
import {
    ProductJugService,
    ProductJugPopupService,
    ProductJugComponent,
    ProductJugDetailComponent,
    ProductJugDialogComponent,
    ProductJugPopupComponent,
    ProductJugDeletePopupComponent,
    ProductJugDeleteDialogComponent,
    productRoute,
    productPopupRoute,
} from './';

const ENTITY_STATES = [
    ...productRoute,
    ...productPopupRoute,
];

@NgModule({
    imports: [
        JugMainzHipsterSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ProductJugComponent,
        ProductJugDetailComponent,
        ProductJugDialogComponent,
        ProductJugDeleteDialogComponent,
        ProductJugPopupComponent,
        ProductJugDeletePopupComponent,
    ],
    entryComponents: [
        ProductJugComponent,
        ProductJugDialogComponent,
        ProductJugPopupComponent,
        ProductJugDeleteDialogComponent,
        ProductJugDeletePopupComponent,
    ],
    providers: [
        ProductJugService,
        ProductJugPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JugMainzHipsterProductJugModule {}
