import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { JugMainzHipsterProductJugModule } from './product-jug/product-jug.module';
import { JugMainzHipsterPartJugModule } from './part-jug/part-jug.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        JugMainzHipsterProductJugModule,
        JugMainzHipsterPartJugModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JugMainzHipsterEntityModule {}
