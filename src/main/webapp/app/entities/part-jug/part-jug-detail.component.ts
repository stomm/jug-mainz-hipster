import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { PartJug } from './part-jug.model';
import { PartJugService } from './part-jug.service';

@Component({
    selector: 'jhi-part-jug-detail',
    templateUrl: './part-jug-detail.component.html'
})
export class PartJugDetailComponent implements OnInit, OnDestroy {

    part: PartJug;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private partService: PartJugService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInParts();
    }

    load(id) {
        this.partService.find(id).subscribe((part) => {
            this.part = part;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInParts() {
        this.eventSubscriber = this.eventManager.subscribe(
            'partListModification',
            (response) => this.load(this.part.id)
        );
    }
}
