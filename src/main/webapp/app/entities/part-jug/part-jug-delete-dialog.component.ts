import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PartJug } from './part-jug.model';
import { PartJugPopupService } from './part-jug-popup.service';
import { PartJugService } from './part-jug.service';

@Component({
    selector: 'jhi-part-jug-delete-dialog',
    templateUrl: './part-jug-delete-dialog.component.html'
})
export class PartJugDeleteDialogComponent {

    part: PartJug;

    constructor(
        private partService: PartJugService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.partService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'partListModification',
                content: 'Deleted an part'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-part-jug-delete-popup',
    template: ''
})
export class PartJugDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private partPopupService: PartJugPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.partPopupService
                .open(PartJugDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
