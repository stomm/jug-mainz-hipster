import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { PartJug } from './part-jug.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PartJugService {

    private resourceUrl =  SERVER_API_URL + 'api/parts';

    constructor(private http: Http) { }

    create(part: PartJug): Observable<PartJug> {
        const copy = this.convert(part);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(part: PartJug): Observable<PartJug> {
        const copy = this.convert(part);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<PartJug> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to PartJug.
     */
    private convertItemFromServer(json: any): PartJug {
        const entity: PartJug = Object.assign(new PartJug(), json);
        return entity;
    }

    /**
     * Convert a PartJug to a JSON which can be sent to the server.
     */
    private convert(part: PartJug): PartJug {
        const copy: PartJug = Object.assign({}, part);
        return copy;
    }
}
