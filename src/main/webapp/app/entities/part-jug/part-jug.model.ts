import { BaseEntity } from './../../shared';

export class PartJug implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public product?: BaseEntity,
    ) {
    }
}
