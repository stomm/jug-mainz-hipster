import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PartJug } from './part-jug.model';
import { PartJugPopupService } from './part-jug-popup.service';
import { PartJugService } from './part-jug.service';
import { ProductJug, ProductJugService } from '../product-jug';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-part-jug-dialog',
    templateUrl: './part-jug-dialog.component.html'
})
export class PartJugDialogComponent implements OnInit {

    part: PartJug;
    isSaving: boolean;

    products: ProductJug[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private partService: PartJugService,
        private productService: ProductJugService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.productService.query()
            .subscribe((res: ResponseWrapper) => { this.products = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.part.id !== undefined) {
            this.subscribeToSaveResponse(
                this.partService.update(this.part));
        } else {
            this.subscribeToSaveResponse(
                this.partService.create(this.part));
        }
    }

    private subscribeToSaveResponse(result: Observable<PartJug>) {
        result.subscribe((res: PartJug) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: PartJug) {
        this.eventManager.broadcast({ name: 'partListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackProductById(index: number, item: ProductJug) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-part-jug-popup',
    template: ''
})
export class PartJugPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private partPopupService: PartJugPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.partPopupService
                    .open(PartJugDialogComponent as Component, params['id']);
            } else {
                this.partPopupService
                    .open(PartJugDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
