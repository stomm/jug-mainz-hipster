import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JugMainzHipsterSharedModule } from '../../shared';
import {
    PartJugService,
    PartJugPopupService,
    PartJugComponent,
    PartJugDetailComponent,
    PartJugDialogComponent,
    PartJugPopupComponent,
    PartJugDeletePopupComponent,
    PartJugDeleteDialogComponent,
    partRoute,
    partPopupRoute,
    PartJugResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...partRoute,
    ...partPopupRoute,
];

@NgModule({
    imports: [
        JugMainzHipsterSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        PartJugComponent,
        PartJugDetailComponent,
        PartJugDialogComponent,
        PartJugDeleteDialogComponent,
        PartJugPopupComponent,
        PartJugDeletePopupComponent,
    ],
    entryComponents: [
        PartJugComponent,
        PartJugDialogComponent,
        PartJugPopupComponent,
        PartJugDeleteDialogComponent,
        PartJugDeletePopupComponent,
    ],
    providers: [
        PartJugService,
        PartJugPopupService,
        PartJugResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JugMainzHipsterPartJugModule {}
