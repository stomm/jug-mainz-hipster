import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { PartJugComponent } from './part-jug.component';
import { PartJugDetailComponent } from './part-jug-detail.component';
import { PartJugPopupComponent } from './part-jug-dialog.component';
import { PartJugDeletePopupComponent } from './part-jug-delete-dialog.component';

@Injectable()
export class PartJugResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const partRoute: Routes = [
    {
        path: 'part-jug',
        component: PartJugComponent,
        resolve: {
            'pagingParams': PartJugResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jugMainzHipsterApp.part.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'part-jug/:id',
        component: PartJugDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jugMainzHipsterApp.part.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const partPopupRoute: Routes = [
    {
        path: 'part-jug-new',
        component: PartJugPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jugMainzHipsterApp.part.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'part-jug/:id/edit',
        component: PartJugPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jugMainzHipsterApp.part.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'part-jug/:id/delete',
        component: PartJugDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jugMainzHipsterApp.part.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
