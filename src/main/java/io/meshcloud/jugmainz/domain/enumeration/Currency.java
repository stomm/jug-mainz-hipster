package io.meshcloud.jugmainz.domain.enumeration;

/**
 * The Currency enumeration.
 */
public enum Currency {
    EUR, USD
}
