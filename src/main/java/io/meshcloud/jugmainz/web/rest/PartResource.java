package io.meshcloud.jugmainz.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.meshcloud.jugmainz.domain.Part;

import io.meshcloud.jugmainz.repository.PartRepository;
import io.meshcloud.jugmainz.web.rest.errors.BadRequestAlertException;
import io.meshcloud.jugmainz.web.rest.util.HeaderUtil;
import io.meshcloud.jugmainz.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Part.
 */
@RestController
@RequestMapping("/api")
public class PartResource {

    private final Logger log = LoggerFactory.getLogger(PartResource.class);

    private static final String ENTITY_NAME = "part";

    private final PartRepository partRepository;

    public PartResource(PartRepository partRepository) {
        this.partRepository = partRepository;
    }

    /**
     * POST  /parts : Create a new part.
     *
     * @param part the part to create
     * @return the ResponseEntity with status 201 (Created) and with body the new part, or with status 400 (Bad Request) if the part has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/parts")
    @Timed
    public ResponseEntity<Part> createPart(@RequestBody Part part) throws URISyntaxException {
        log.debug("REST request to save Part : {}", part);
        if (part.getId() != null) {
            throw new BadRequestAlertException("A new part cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Part result = partRepository.save(part);
        return ResponseEntity.created(new URI("/api/parts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /parts : Updates an existing part.
     *
     * @param part the part to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated part,
     * or with status 400 (Bad Request) if the part is not valid,
     * or with status 500 (Internal Server Error) if the part couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/parts")
    @Timed
    public ResponseEntity<Part> updatePart(@RequestBody Part part) throws URISyntaxException {
        log.debug("REST request to update Part : {}", part);
        if (part.getId() == null) {
            return createPart(part);
        }
        Part result = partRepository.save(part);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, part.getId().toString()))
            .body(result);
    }

    /**
     * GET  /parts : get all the parts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of parts in body
     */
    @GetMapping("/parts")
    @Timed
    public ResponseEntity<List<Part>> getAllParts(Pageable pageable) {
        log.debug("REST request to get a page of Parts");
        Page<Part> page = partRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/parts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /parts/:id : get the "id" part.
     *
     * @param id the id of the part to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the part, or with status 404 (Not Found)
     */
    @GetMapping("/parts/{id}")
    @Timed
    public ResponseEntity<Part> getPart(@PathVariable Long id) {
        log.debug("REST request to get Part : {}", id);
        Part part = partRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(part));
    }

    /**
     * DELETE  /parts/:id : delete the "id" part.
     *
     * @param id the id of the part to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/parts/{id}")
    @Timed
    public ResponseEntity<Void> deletePart(@PathVariable Long id) {
        log.debug("REST request to delete Part : {}", id);
        partRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
