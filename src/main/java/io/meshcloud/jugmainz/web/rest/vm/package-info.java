/**
 * View Models used by Spring MVC REST controllers.
 */
package io.meshcloud.jugmainz.web.rest.vm;
