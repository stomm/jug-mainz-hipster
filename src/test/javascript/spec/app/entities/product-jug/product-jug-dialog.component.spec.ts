/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { JugMainzHipsterTestModule } from '../../../test.module';
import { ProductJugDialogComponent } from '../../../../../../main/webapp/app/entities/product-jug/product-jug-dialog.component';
import { ProductJugService } from '../../../../../../main/webapp/app/entities/product-jug/product-jug.service';
import { ProductJug } from '../../../../../../main/webapp/app/entities/product-jug/product-jug.model';

describe('Component Tests', () => {

    describe('ProductJug Management Dialog Component', () => {
        let comp: ProductJugDialogComponent;
        let fixture: ComponentFixture<ProductJugDialogComponent>;
        let service: ProductJugService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JugMainzHipsterTestModule],
                declarations: [ProductJugDialogComponent],
                providers: [
                    ProductJugService
                ]
            })
            .overrideTemplate(ProductJugDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ProductJugDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProductJugService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ProductJug(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(entity));
                        comp.product = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'productListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ProductJug();
                        spyOn(service, 'create').and.returnValue(Observable.of(entity));
                        comp.product = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'productListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
