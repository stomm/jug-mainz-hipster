/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';

import { JugMainzHipsterTestModule } from '../../../test.module';
import { ProductJugDetailComponent } from '../../../../../../main/webapp/app/entities/product-jug/product-jug-detail.component';
import { ProductJugService } from '../../../../../../main/webapp/app/entities/product-jug/product-jug.service';
import { ProductJug } from '../../../../../../main/webapp/app/entities/product-jug/product-jug.model';

describe('Component Tests', () => {

    describe('ProductJug Management Detail Component', () => {
        let comp: ProductJugDetailComponent;
        let fixture: ComponentFixture<ProductJugDetailComponent>;
        let service: ProductJugService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JugMainzHipsterTestModule],
                declarations: [ProductJugDetailComponent],
                providers: [
                    ProductJugService
                ]
            })
            .overrideTemplate(ProductJugDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ProductJugDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProductJugService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new ProductJug(123)));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.product).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
