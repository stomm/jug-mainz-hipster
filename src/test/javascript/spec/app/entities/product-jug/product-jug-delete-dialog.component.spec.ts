/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { JugMainzHipsterTestModule } from '../../../test.module';
import { ProductJugDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/product-jug/product-jug-delete-dialog.component';
import { ProductJugService } from '../../../../../../main/webapp/app/entities/product-jug/product-jug.service';

describe('Component Tests', () => {

    describe('ProductJug Management Delete Component', () => {
        let comp: ProductJugDeleteDialogComponent;
        let fixture: ComponentFixture<ProductJugDeleteDialogComponent>;
        let service: ProductJugService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JugMainzHipsterTestModule],
                declarations: [ProductJugDeleteDialogComponent],
                providers: [
                    ProductJugService
                ]
            })
            .overrideTemplate(ProductJugDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ProductJugDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProductJugService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
