/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { Headers } from '@angular/http';

import { JugMainzHipsterTestModule } from '../../../test.module';
import { ProductJugComponent } from '../../../../../../main/webapp/app/entities/product-jug/product-jug.component';
import { ProductJugService } from '../../../../../../main/webapp/app/entities/product-jug/product-jug.service';
import { ProductJug } from '../../../../../../main/webapp/app/entities/product-jug/product-jug.model';

describe('Component Tests', () => {

    describe('ProductJug Management Component', () => {
        let comp: ProductJugComponent;
        let fixture: ComponentFixture<ProductJugComponent>;
        let service: ProductJugService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JugMainzHipsterTestModule],
                declarations: [ProductJugComponent],
                providers: [
                    ProductJugService
                ]
            })
            .overrideTemplate(ProductJugComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ProductJugComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProductJugService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new ProductJug(123)],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.products[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
