/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';

import { JugMainzHipsterTestModule } from '../../../test.module';
import { PartJugDetailComponent } from '../../../../../../main/webapp/app/entities/part-jug/part-jug-detail.component';
import { PartJugService } from '../../../../../../main/webapp/app/entities/part-jug/part-jug.service';
import { PartJug } from '../../../../../../main/webapp/app/entities/part-jug/part-jug.model';

describe('Component Tests', () => {

    describe('PartJug Management Detail Component', () => {
        let comp: PartJugDetailComponent;
        let fixture: ComponentFixture<PartJugDetailComponent>;
        let service: PartJugService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JugMainzHipsterTestModule],
                declarations: [PartJugDetailComponent],
                providers: [
                    PartJugService
                ]
            })
            .overrideTemplate(PartJugDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PartJugDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PartJugService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new PartJug(123)));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.part).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
