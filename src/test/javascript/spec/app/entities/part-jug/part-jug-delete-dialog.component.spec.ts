/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { JugMainzHipsterTestModule } from '../../../test.module';
import { PartJugDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/part-jug/part-jug-delete-dialog.component';
import { PartJugService } from '../../../../../../main/webapp/app/entities/part-jug/part-jug.service';

describe('Component Tests', () => {

    describe('PartJug Management Delete Component', () => {
        let comp: PartJugDeleteDialogComponent;
        let fixture: ComponentFixture<PartJugDeleteDialogComponent>;
        let service: PartJugService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JugMainzHipsterTestModule],
                declarations: [PartJugDeleteDialogComponent],
                providers: [
                    PartJugService
                ]
            })
            .overrideTemplate(PartJugDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PartJugDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PartJugService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
