/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { JugMainzHipsterTestModule } from '../../../test.module';
import { PartJugDialogComponent } from '../../../../../../main/webapp/app/entities/part-jug/part-jug-dialog.component';
import { PartJugService } from '../../../../../../main/webapp/app/entities/part-jug/part-jug.service';
import { PartJug } from '../../../../../../main/webapp/app/entities/part-jug/part-jug.model';
import { ProductJugService } from '../../../../../../main/webapp/app/entities/product-jug';

describe('Component Tests', () => {

    describe('PartJug Management Dialog Component', () => {
        let comp: PartJugDialogComponent;
        let fixture: ComponentFixture<PartJugDialogComponent>;
        let service: PartJugService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JugMainzHipsterTestModule],
                declarations: [PartJugDialogComponent],
                providers: [
                    ProductJugService,
                    PartJugService
                ]
            })
            .overrideTemplate(PartJugDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PartJugDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PartJugService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new PartJug(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(entity));
                        comp.part = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'partListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new PartJug();
                        spyOn(service, 'create').and.returnValue(Observable.of(entity));
                        comp.part = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'partListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
