/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { Headers } from '@angular/http';

import { JugMainzHipsterTestModule } from '../../../test.module';
import { PartJugComponent } from '../../../../../../main/webapp/app/entities/part-jug/part-jug.component';
import { PartJugService } from '../../../../../../main/webapp/app/entities/part-jug/part-jug.service';
import { PartJug } from '../../../../../../main/webapp/app/entities/part-jug/part-jug.model';

describe('Component Tests', () => {

    describe('PartJug Management Component', () => {
        let comp: PartJugComponent;
        let fixture: ComponentFixture<PartJugComponent>;
        let service: PartJugService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JugMainzHipsterTestModule],
                declarations: [PartJugComponent],
                providers: [
                    PartJugService
                ]
            })
            .overrideTemplate(PartJugComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PartJugComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PartJugService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new PartJug(123)],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.parts[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
